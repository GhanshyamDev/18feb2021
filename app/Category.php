<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
   protected $table ='tbl_category';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable =['category' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
     
 
}
