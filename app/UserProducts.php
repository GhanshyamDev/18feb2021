<?php

namespace App;
 
use Illuminate\Database\Eloquent\Model;

class UserProducts extends Model
{
  //edit new changes
  protected $table ='tbl_user_products';
    
    protected $fillable = [ 'product_id', 'assign_by', 'userid', 'category_id', 'product_warranty', 'warranty_start', 'warranty_end', 'pauchase_date', 'document' ];

     
   public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'product_name'
            ]
        ];
    }
     public $timestamps = true;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
   
 
}
