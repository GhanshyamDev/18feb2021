<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
      protected $table ='tbl_states';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'state_id','state_name', 
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    
 
}
