<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\Products;
use App\UserProducts;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
class TicketController extends Controller
{
    
 
     public function index($id) {
       
      $product  = Products::where('product_id', Crypt::decryptString($id))->first();
      $data['product_id'] = $product['product_id'];
      $data['userProductDetails'] = UserProducts::join('tbl_products as p', 'p.product_id', '=', 'tbl_user_products.product_id')
            ->join('tbl_category as cat', 'cat.category_id', '=', 'tbl_user_products.category_id')
            ->where('p.product_id', '=', Crypt::decryptString($id))
            ->where('tbl_user_products.product_id', '=', $product['product_id'])
              ->first();
      return view('ticket.create',$data);
     }
     

     public function postTicket(Request $request) {
          
         $request->validate([
            'service_type' => 'required',
            'description' => 'required',
        ]);
           
           $dataArray  =    array(
            "send_by"    =>    session('Customer_logged')['cust_id'],
            "product_id"    => $request->product_id,
            "type"       =>    $request->service_type, 
            "message"    =>    $request->description ,
            "where_to"   =>    $request->where_to, 
            "created_at" =>    now()
           );

            $ticket  = DB::table('user_request')->insert($dataArray);
            if($ticket) {

		         return redirect('users/dashboard')->with('success', 'Send Request Successfully');

	            } else {

	              return back()->with("failed", "Failed to Send Request");
	            }
           
          // return back()->with("failed", 'Oppes! You have entered invalid credentials');
 		
     }

     public function markTicket(Request $request){
      
      return Db::table('tickets')->where('ticket_id', $request->ticket_id)->update(['read_at' => now()]);

     }
      public function markAllTicket(Request $request){
      return Db::table('tickets')->update(['read_at' => now()]);

     }

       public function viewTicket() {
             return view('subAdmin.view-ticket');
        }
       public function Adminticket() {
            
         $data['ticketDetails'] = Ticket::join('customer as c', 'c.cust_id', '=', 'tickets.oem_cust_id')
             ->get()->toArray();
       
 
        return view('admin.view-ticket', $data);
     
       }


       public function Oemticket() {
               $data['ticketDetails'] = Ticket::join('customer as c', 'c.cust_id', '=', 'tickets.oem_cust_id')
             ->get()->toArray();
             return view('subAdmin.view-ticket', $data);
     
       } 

// Create service Request

   public function CreateService($id){
 
      $product  = Products::where('product_id', Crypt::decryptString($id))->first();
      $data['product_id'] = $product['product_id'];
      $data['userProductDetails'] = UserProducts::join('tbl_products as p', 'p.product_id', '=', 'tbl_user_products.product_id')
            ->join('tbl_category as cat', 'cat.category_id', '=', 'tbl_user_products.category_id')
            ->where('p.product_id', '=', Crypt::decryptString($id))
            ->where('tbl_user_products.product_id', '=', $product['product_id'])
              ->first();

      return view ('ticket.create-service', $data);
   }  


   public function openCloseTicket(Request $request){
     
     if($request->value==1)
     {
      $data = ['read_at' => now()];
     }else
      { $data = ['read_at' => NULL];  }
   
   $status = DB::table('user_request')->where('req_id', $request->id)->update($data);
   if ($status) {
      return response()->json(['success'=> 200, 'mesg' => 'Status change successfully.']);
   }
      return response()->json(['success'=> 300, 'mesg' => 'Request Failed.']);
   } 
}
