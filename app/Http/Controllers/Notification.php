<?php

namespace App\Http\Controllers;

use App\Notifications\NotificationCreated;
use Illuminate\Http\Request;
use App\Notifications\NewPostNotify;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Auth;
use App\User;
use App\Category;
use App\Products;
use Illuminate\Support\Facades\DB;
class Notification extends Controller
{ 
     public function sendServiceNotification(Request $request)
      {    

      	 $users = User::first();
      	    $user = [
      	     'user_id' =>  Auth::user()->id,
      	     'name'   => 'Sonu Mishra',
      	     'email' =>  'ss@gmail.com',	
      	     ];
         $users->notify(new NotificationCreated($user));
          notify()->preset('user-updated');
      	     //  notify()->preset('user-updated');
           session::flash('success','Notification Send Successfully');
            return redirect()->back();
      }

 public function markNotification(Request $request)
    {
       
        $update= ['read_at' =>now(), ];
         DB::table('notifications')->where('id', $request->input('id'))->update($update);
        return response('200');
       
   }

  public function markAllNotification(Request $request) { 
     
      $update= ['read_at' =>now(), ];
         DB::table('notifications')->update($update);
         return response('200');
  }



   public function displayNotification(){
  	    
       $user = User::find(1);
        $data['notifications'] = $user->unreadNotifications;
           return view('notification',$data);
 
  }

// Send Notification to User

  public function  sendUserNotification(Request $request){
      
      if ($request->isMethod('post')) { 
          $request->validate([
              'category' => 'required',
              'message' => 'required',
          ]);

             $user = [
               'send_by' =>auth()->user()->id,
               'type' => $request->type,
               'Send_product_user' => $request->category,
               'message' => $request->message,
               'created_at'  =>now()
              ];
                $users = DB::table('tbl_notification')->insert($user);
              if($users){
                  return back()->with("success", "Notification Send Successfully");
              } else{
                  return back()->with("failed", "Notification Send Failed");
              }
        
          
       } else {
      
        $data['products'] = Products::get();
        return view('userNotification', $data); 
           
      }    
  }

}  