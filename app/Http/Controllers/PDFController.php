<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use PDF;
use App\State;
use App\City;
use App\UserProducts;
class PDFController extends Controller
{
    function pdf($id)
    {
     $pdf = \App::make('dompdf.wrapper');
     $pdf->loadHTML($this->convert_customer_data_to_html($id));
     return $pdf->stream();
    }

     function get_customer_data($id)
    {
      
         $customer_data = UserProducts::join('tbl_products as p', 'p.product_id', '=', 'tbl_user_products.product_id')
        ->join('tbl_category as cat', 'cat.category_id', '=', 'tbl_user_products.category_id')
        ->join('customer as cust', 'cust.cust_id', '=' ,'tbl_user_products.userid')
        ->where('p.product_id', '=', $id)
        ->where('tbl_user_products.product_id', '=', $id)
        ->get();
     return $customer_data;
    }
     function convert_customer_data_to_html($id)
    {
    $output='';
    $customer_data = $this->get_customer_data($id);
     $output = '
     <h3 align="center">User Product Details</h3>
     <table width="100%" style="border-collapse: collapse; border: 0px;">
      <tr>
    <th style="border-bottom: 1px solid; padding:12px;" width="50%">User Details</th>
    <th style="border-bottom: 1px solid; padding:12px;" width="50%">Product Details</th>
     
   </tr>';
     foreach($customer_data as $customer)
     { 
     	  $city = City::where('city_id', $customer->city)->get();
          $data['state'] = State::where('state_id', $customer->state)->get();   
         
       $output .= '
				<tr style="margine-top:-50px;"><td > 
				    <h4>Name. '.$customer->name.'</h4>
					<h4>Email. '.$customer->email.'</h4>
					<h4>Aadhar. XXXXXXXX</h4>
					<h4>Mobile. '.$customer->mobile.'</h4>
					<h4>Location. Rajasthan, Kota, 24005</h4>
				 </td>
				 ';
				 $output .= '
					<td> 
					  <h4>Name. '.$customer->product_name.'</h4>
						<h4>Price. '.$customer->product_price.'</h4>
						<h4>Aadhar. XXXXXXXX</h4>
						<h4>Braand. '.$customer->category.'</h4>
						<h4>Purchase Date. '.date('d M Y', strtotime($customer->pauchase_date)).'</h4>
						<h4>Warranty  Date. '.date('d M Y', strtotime($customer->warranty_start)).'</h4>
						<h4>Warranty Start. '.date('d M Y', strtotime($customer->warranty_end)).'</h4>
					 </td>
					 </tr> ';
     }
     $output .= '</tbody></table>';
     return $output;
    }
}
