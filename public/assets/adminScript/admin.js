
 
   $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  y})


  $(function() {

    $('.ApproveUser').change(function() {

        var value = $(this).prop('checked') == true ? 'Success' : 'Pending'; 
        var oemuid = $(this).data('id');
         $("#approveMsg").html(" ");
          var confrm = confirm("Are You Sure...!");
     
    if(confrm == true) {
         $.ajax({
             type: "POST",
             url: '{{ url('approveUser') }}',
             data: {'_token': '{{ csrf_token() }}', 'oemuid': oemuid, 'value': value},
            success: function(response){
                if(response['success'] == true ){
                     if(value == 'Success') { var stats = 'Active';} else{  var stats = 'InActive'; }
                  $("#approveMsg").html('<div class="alert alert-success"><strong>'+stats+'!</strong>  User '+stats+' Successfully</div>');
                } else {
                    $("#approveMsg").html(' <div class="alert alert-success"> <strong>Failed!</strong> '+response['data']+' </div>');
                }
            }
       });
     }
    })
  })


  $(function() {
    $('.closeOpenTicket').change(function() {
        var val = $(this).prop('checked') == true ? 1 : 0; 
        var id = $(this).data('id'); 
         
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{ url('opencloseTicket') }}",
            data: {'_token': '{{ csrf_token() }}', 'id': id, 'value': val},
            success: function(data){
               $("row_"+id).reload();

            }
        });
    })
  })
  // view ticket
 $(document).on('click', '#smallButton', function(event) {
            event.preventDefault();
          
        });


  $('#viewTicket').click(function() {
    var id =$(this).data('id');
      
           let user = $(this).text();
           let message = $('#message_'+id).text();  

             $('#userName').text(user);  
             $('#usermessage').text(message);  
              $('#ViewTicketModal').modal("show");
      })
 
    $(document).ready(function() {
         $('.exampleButtons').DataTable({
             dom: 'Bfrtip',
             buttons: [ 'excel']
         });
    } );
    
$(document).ready(function() {
        var table = $('.example').DataTable( {
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
        } );     
    $('viewAmcRequest').on('click', 'tr', function () {
        var data = table.row( this ).data();
        alert( 'You clicked on '+data+'\'s row' );
    } );
} );



    $(document).ready(function() {
        var table = $('#example').DataTable( {

            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
        } );
     
        // table.buttons().container()
        //       .insertBefore( '#example_filter' );
    });
   
 
   function sendMarkTicket($id) {
 
       $.ajax({
            url: "{{ route('sendMarkTicket') }}",
            type:"POST",
            data: {
               "_token": "{{ csrf_token() }}",
               ticket_id:$id
             },
            success:function(response){
               $("#row_"+$id).remove();
               //$(this).parents('div.alert').remove();
            },
           }); 
    }
    function markAllTicket() {

       $.ajax({
            url: "{{ route('markAllTicket') }}",
            type:"POST",
            data: {
               "_token": "{{ csrf_token() }}",
               all:'all'
             },
            success:function(response){
               $("#allbody").remove();
               //$(this).parents('div.alert').remove();
            },
           }); 
    }
 