  <!DOCTYPE html>
<html lang="en"> 
  <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Clyfe</title>
    @notifyCss
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.base.css') }}">
    <!-- Start datatables -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> 
     <link rel="stylesheet" href="{{ asset('assets/dataTable/dataTables.bootstrap4.min.css') }}"> 

 <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">


       <!-- End  datatables --> 
    <link rel="stylesheet" href=" {{ asset('assets/css/style.css') }}">
    <!-- End layout styles -->
    <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" />
    <link rel="stylesheet" href="{{ asset('assets/select2/css/select2.min.css') }}">
   <link rel="stylesheet" href="{{ asset('assets/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
   
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center" style="border-bottom:1px solid #eaeaea9c;">
          @if(auth()->user()->is_admin == 1) 
       <a class="navbar-brand brand-logo"  href="#"><img src="{{ asset('img/clyfe.png')  }}" ></a>
          <a class="navbar-brand brand-logo-mini" href="#"><img src="{{ asset('img/favicon.png') }}" alt="logo" /></a>
          @else 
            <a class="navbar-brand brand-logo" href="#"><img style="height: 80px !important;" src="{{ asset('img/category') }}/{{ Auth::user()->avatar }}" ></a>
          <a class="navbar-brand brand-logo-mini" href="#"><img src="{{ asset('img/favicon.png') }}" alt="logo" /></a>
          @endif 
          
       

        </div>
        <div class="navbar-menu-wrapper d-flex align-items-stretch">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
          </button>
          <div class="search-field d-none d-md-block">
            <form class="d-flex align-items-center h-100" action="#">
               
            </form>
          </div>
          <ul class="navbar-nav navbar-nav-right">
           
             <li class="nav-item dropdown">
                @if(auth()->user()->is_admin == 1) 

                @else
                <img src="{{ asset('img/clyfe.png') }}" style="width: 120px;">
                @endif
               
               
            </li>
            <li class="nav-item dropdown">
              <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
                <i class="mdi mdi-bell-outline"></i>
                <span class="count-symbol bg-danger"></span>
              </a>
               
            </li>
             
             <li class="nav-item nav-profile dropdown">
              <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <!-- <div class="nav-profile-img">
                  <img src="{{ asset('assets/images/faces/face1.jpg') }}" alt="image">
                  <span class="availability-status online"></span>
                </div> -->
                <div class="nav-profile-text">
                  <p class="mb-1 text-black">{{ Auth::user()->name }}</p>
                </div>
              </a>
              <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
                <a class="dropdown-item" href="{{ url('oemAdmin/my-account')}}">
                  <i class="mdi mdi-cached mr-2 text-success"></i> My Account</a>
                <div class="dropdown-divider"></div>
                 
                <a class="dropdown-item"href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
               <i class="mdi mdi-logout mr-2 text-primary"></i>  {{ __('Logout') }}
                </a>
                 <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf    </form>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
            @if(auth()->user()->is_admin == 1) 
             
              <li class="nav-item">
                <a class="nav-link" href="{{ url('admin/dashboard') }}">
                  <span class="menu-title">Dashboard</span>
                  <i class="mdi mdi-home menu-icon"></i>
                </a>
             </li>
 
            @else
             
            <li class="nav-item">
              <a class="nav-link" href="{{ route('home') }}">
                <span class="menu-title">Dashboard</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
            
            <li class="nav-item">
              <a class="nav-link" href="{{ url('oemAdmin/createCategory') }}">
                <span class="menu-title">Create Category</span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>

             <li class="nav-item">
              <a class="nav-link" href="{{ url('oemAdmin/createGeography') }}">
                <span class="menu-title">Create Geography </span><i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>

             <li class="nav-item">
              <a class="nav-link"  href="{{ route('oemAdmin.userCreate') }}">
                <span class="menu-title">Create User </span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>
 
           <li class="nav-item">
              <a class="nav-link"  href="{{ url('oemAdmin/viewTicket') }}">
                <span class="menu-title">View Ticket </span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>

            <!-- <li class="nav-item">
              <a class="nav-link"  href="{{ route('sendServiceReqAll') }}" onclick="event.preventDefault(); document.getElementById('Send-Service-notification-form').submit();">
                <span class="menu-title">Send Notification</span> <i class="mdi mdi-send menu-icon"></i>
              </a>
              <form id="Send-Service-notification-form" action="{{ route('sendServiceReqAll') }}" method="POST" style="display: none;">
               @csrf </form> 
            </li> -->

            <li class="nav-item">
              <a class="nav-link"  href="{{ url('oemAdmin/ViewRenewAmc') }}">
                <span class="menu-title">Renew AMC</span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>

             <li class="nav-item">
              <a class="nav-link"  href="{{ url('oemAdmin/ViewServiceRequest') }}">
                <span class="menu-title">View Service Request</span> <i class="mdi mdi-arrow-right menu-icon"></i>
              </a>
            </li>

            @endif
             <li class="nav-item">
                <a class="nav-link" href="{{ url('userNotification') }}">
                  <span class="menu-title">Send Notification</span>
                  <i class="mdi mdi-arrow-right menu-icon"></i>
                </a>
             </li>
          </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
        
            @yield('content')
        </div>


        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>

<!--     //Modal --> 


<!-- medium modal -->


<script src="{{ asset('assets/jquery/jquery-3.5.1.js') }}"></script> 
<script src="{{ asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('assets/js/off-canvas.js') }}"></script>
<script src="{{ asset('assets/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('assets/js/misc.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
<script src="{{ asset('assets/js/file-upload.js') }}"></script>
 <script src="{{ asset('assets/select2/js/select2.full.min.js') }}"></script>
 <script src="{{ asset('assets/dataTable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/dataTable/dataTables.bootstrap4.min.js') }}"></script>
<!-- <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.semanticui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.colVis.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
 

<script type="text/javascript">
   $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
})
</script>
 
  
    <script type="text/javascript">
      $(function() {

    $('.ApproveUser').change(function() {

        var value = $(this).prop('checked') == true ? 'Success' : 'Pending'; 
        var oemuid = $(this).data('id');
         $("#approveMsg").html(" ");
          var confrm = confirm("Are You Sure...!");
     
    if(confrm == true) {
         $.ajax({
             type: "POST",
             url: '{{ url('approveUser') }}',
             data: {'_token': '{{ csrf_token() }}', 'oemuid': oemuid, 'value': value},
            success: function(response){
                if(response['success'] == true ){
                     if(value == 'Success') { var stats = 'Active';} else{  var stats = 'InActive'; }
                  $("#approveMsg").html('<div class="alert alert-success"><strong>'+stats+'!</strong>  User '+stats+' Successfully</div>');
                } else {
                    $("#approveMsg").html(' <div class="alert alert-success"> <strong>Failed!</strong> '+response['data']+' </div>');
                }
            }
       });
     }
    })
  })


  $(function() {
    $('.closeOpenTicket').change(function() {
        var val = $(this).prop('checked') == true ? 1 : 0; 
        var id = $(this).data('id'); 
         
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "{{ url('opencloseTicket') }}",
            data: {'_token': '{{ csrf_token() }}', 'id': id, 'value': val},
            success: function(response){

              if(response['success'] == 200 ){
                  $('.disablebtn').prop('disabled', true);
              }else {
                 alert(response['success']);
              }
                 
              
            }
        });
    })
  })
  // view ticket
 

  $('#viewTicket').click(function() {
    var id =$(this).data('id');
      
           let user = $(this).text();
           let message = $('#message_'+id).text();  
           let read = $('#read_'+id).text();  
          
             $('#productName').val(user);  
             $('#usermessage').val(message);  
              $('#ViewTicketModal').modal("show");
      })
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
         $('.exampleButtons').DataTable({
             dom: 'Bfrtip',
             buttons: [ 'excel']
         });
    } );
    
$(document).ready(function() {
        var table = $('.example').DataTable( {
        buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
        } );     
    $('viewAmcRequest').on('click', 'tr', function () {
        var data = table.row( this ).data();
        alert( 'You clicked on '+data+'\'s row' );
    } );
} );



    $(document).ready(function() {
        var table = $('#example').DataTable( {

            buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
        } );
     
        table.buttons().container()
              .insertBefore( '#example_filter' );
    });
   
</script>  
  <script type="text/javascript">
    // view all reaports
 $("select[name='id_type']").change(function(){
      var id_type = $(this).val();
      alert(id_type);
      
      $.ajax({
          url: "{{ route('select-serviceType') }}",
          method: 'POST',
         data: {
               "_token": "{{ csrf_token() }}",
                type:id_type
             },
          success: function(data) {
            alert(data);
          }
      });
  });
</script> 
<script type="text/javascript">
   function sendMarkTicket($id) {
alert($id);
       $.ajax({
            url: "{{ route('sendMarkTicket') }}",
            type:"POST",
            data: {
               "_token": "{{ csrf_token() }}",
               ticket_id:$id
             },
            success:function(response){
               $("#row_"+$id).remove();
               //$(this).parents('div.alert').remove();
            },
           }); 
    }
    function markAllTicket() {

       $.ajax({
            url: "{{ route('markAllTicket') }}",
            type:"POST",
            data: {
               "_token": "{{ csrf_token() }}",
               all:'all'
             },
            success:function(response){
               $("#allbody").remove();
               //$(this).parents('div.alert').remove();
            },
           }); 
    }
</script>
  @include('notify::messages')
        
  <x:notify-messages />
  @notifyJs
  </body>
</html>
