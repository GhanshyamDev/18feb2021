@extends('layouts.customer')

@section('content') 
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h2 class="m-0">   Dashboard</h2>
          </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header --> 
   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
         
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                      
                  @if(Session::has('success'))
                  <div class="alert alert-success">
                    <strong>Success!</strong> {{Session::get('success')}}
                  </div>
                  @endif
                  @if(Session::has('failed'))
                  <div class="alert alert-success">
                    <strong>Failed!</strong> {{Session::get('failed')}}
                  </div>
                  @endif 
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                     <tr>
                      <th>Sr NO</th>
                      <th>Product Name</th>
                      <th>Manufacturer</th>
                      <th>Pauchase Date</th>
                      <th>Warranty</th>
                      <th>View Details</th>
                     </tr>
                  </thead>
                
                   <tbody>
                         @if (count($userProduct)) 

                           @foreach ($userProduct as $info)  
                            <tr>
                                <td>{{ $info['user_pr_id'] }}</td>
                                <td>{{ $info['product_name'] }}</td>
                                <td> <ul class="list-inline">
                  <li class="list-inline-item">
                      <img alt="Avatar" class="table-avatar" style="width: 40px;" src="{{ asset('img/'.$info['logo'] ) }}">
                  {{ $info['category'] }}</li>  </ul></td>
                                <td>{{ date('d M Y', strtotime($info['pauchase_date']))  }} </td>
                               <?php 
                              $to = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $info['warranty_start']);
                                $from = \Carbon\Carbon::createFromFormat('Y-m-d H:s:i', $info['warranty_end']);
                                $diff_in_days = $to->diffInDays($from);
                                if($diff_in_days == 1)
                                  { ?> 
                               <td>{{ $diff_in_days  }} Days <span class="badge badge-warning">Expire Soon</span></td>
                                <?php  }  elseif($diff_in_days == 0) { ?>
                               <td>{{ $diff_in_days  }} Days <span class="badge badge-danger">Expire </span></td>
                                 <?php  }  else{ ?>
                                 <td>{{ $diff_in_days  }} Days <span class="badge badge-success">In_warranty</span></td>
                                <?php } ?>
                                <td> <a  href="{{ url('users/product') }}/{{ Crypt::encryptString($info['product_id']) }}" class="btn btn-primary btn-xs" href="#"><i class="fas fa-folder"> </i> View</a>             
                               </td>
                               
                            </tr>
                               @endforeach
                             @else
                                 
                             @endif 
                        </tbody>
                         
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
