<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
 <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon.png') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('User Login') }}</title>
 
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
   <link rel="stylesheet" href="{{ asset('assets/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/icheck-bootstrap/icheck-bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- Scripts -->
 
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
<style>
    .bs-example{
      margin: 20px;
        position: relative;
    }
</style>
<script>
    $(document).ready(function(){
        $(".show-toast").click(function(){
            $("#myToast").toast('show');
        });
    });
</script>
    <!-- Styles -->
   <!--  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card ">
    <div class="card-header text-center">
        <div class="brand-logo text-center">
          <img src="{{ asset('img/clyfe.png') }}">
        </div>
    </div>
    <div class="card-body">
        <div class="card-header">
           @if(Session::has('failed'))
             <span style="color:  #d32535;"> {{Session::get('failed')}}   </span>
           @endif
            @if(!empty($error))
             <span style="color:  #d32535;"> {{ $error }}   </span>
           @endif
        </div>
       <form method="POST" action="{{ url('users/authenticate') }}">
       @csrf
       <div class="input-group mb-1">
          <input type="email" class="form-control @error('email') is-invalid @enderror"   name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
         </div>
          @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
             
          <div class="input-group pt-3 mb-1">
           <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas fa-lock"></span>
              </div>
            </div>
          </div>
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror

       
         <div class="text-center mt-4 font-weight-light"> 

            <button type="submit" class="btn btn-primary btn-block" style="width:150px;">  {{ __('Sign In') }}  </button>
        </div>
      </form>

     <!--  <div class="social-auth-links text-center mt-2 mb-3">
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div> -->
       
         <p class="mb-0"><a href="{{ url('users/register') }}" class="text-center" >{{ __('Register') }}</a></p>
         
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
  <div class="toast" id="myToast" style="position: absolute; top: 0; right: 0;">
              <div class="toast-header">
                  <strong class="mr-auto"><i class="fa fa-grav"></i> We miss you!</strong>
                  <small>11 mins ago</small>
                  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">
                      <span aria-hidden="true">&times;</span>
                  </button>
              </div>
              <div class="toast-body">
                  <div>It's been a long time since you visited us. We've something special for you. <a href="#">Click here!</a></div>
              </div>
          </div>
<script src=" {{ asset('assets/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src=" {{ asset('dist/js/adminlte.js') }}"></script>
</body>
</html>
