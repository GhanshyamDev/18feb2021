@extends('layouts.customer')

@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-sm-6">
            <h2 class="mb-2">Create Service Request</h2>
          </div><!-- /.col -->
         <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header --> 
   <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
          
        <!-- /.row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
        
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
              
              <div class="row">
                <div class="col-12">
                  
                  <div class="post">
                     
                  <form method="POST" action="{{ route('users.postTicket') }}">
                    {{ csrf_field() }}
                    <div class="card-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Renew Service</label>
                        <input type="hidden" name="where_to" value="User_Create_Service">
                        <input type="hidden" name="product_id" value="{{ $product_id }}">

                        <select class="form-control"  name="service_type"  >
                        <option value=" ">Select Service</option>
                           <option value="AMC">AMC</option>
                          <option value="EXTENDED_WARRANTY">Extended Warranty</option>
                       </select>
                        @if ($errors->has('service_type'))
                            <span class="text-danger">{{ $errors->first('service_type') }}</span>
                            @endif
                      </div>
                      <div class="form-group">
                        <label for="exampleInputPassword1">Message(<span style="color: #db5252;font-size:15px;">Max. 100 char required</span>)</label>
                        <textarea  name="description" maxlength="100" class="form-control textareaCount"  ></textarea>
                         @if ($errors->has('description'))
                            <span class="text-danger">{{ $errors->first('description') }}</span>
                            @endif
                      </div>
                      
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer">
                     <input type="submit" class="btn btn-primary btn-block" name="submit" value="Send">
                    </div>
                  </form>
                   <script type="text/javascript">
                        $('textarea').maxlength({
                                 alwaysShow: true,
                                threshold: 10,
                                warningClass: "label label-success",
                                limitReachedClass: "label label-danger",
                                separator: ' out of ',
                                preText: 'You write ',
                                postText: ' chars.',
                                validate: true
                            });
                      </script>
                  </div>
                </div>
              </div>
            </div>
             
          </div>
        </div>
        <!-- /.card-body -->
      </div>
             
          </div>
          <!-- /.col -->
        </div>
        <!-- Main row -->
        
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
