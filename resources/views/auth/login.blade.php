<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 <link rel="icon" type="image/icon" sizes="32x32" href="{{ asset('img/favicon.png') }}">
    <!-- CSRF Token -->
    
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Clyfe</title>
 
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
   <link rel="stylesheet" href="{{ asset('assets/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/icheck-bootstrap/icheck-bootstrap.min.css') }}">
      <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- Scripts -->
  <!--   <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
   <!--  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card  ">
    <div class="card-header text-center p-3" style="border-bottom:none !important">
       <div class="brand-logo text-center">
          <img src="{{ asset('img/clyfe.png') }}">
        </div>
    </div>
    <div class="card-body">
     

      <form method="POST" action="{{ route('login') }}">
       @csrf
       <div class="input-group mb-1">
          <input type="email" class="form-control @error('email') is-invalid @enderror"   name="email" value="{{ old('email') }}" placeholder="Enter Email" required autocomplete="email" autofocus>
          
         </div>
          @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
            @enderror
             
        <div class="input-group pt-3 mb-1">
         <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Enter Password" required autocomplete="current-password">
           
        </div>
        @error('password')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
        <div class="row">
          <div class="col-8">
           <!--  <div class="icheck-primary ml-3">
                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
               
              <label for="remember ">
                {{ __('Remember Me') }}
              </label>
            </div> -->
          </div>
          <!-- /.col -->
          <div class="col-4">
             
          </div>

          <!-- /.col -->
        </div>
        <div class="social-auth-links text-center mt-2 mb-3">
         <button type="submit" class="btn btn-primary btn-block" style="background:#2c349c;border-color:#2c349c;width: 80px;">  {{ __('Sign In') }}  </button>
        </div>
      </form>

     <!--  <div class="social-auth-links text-center mt-2 mb-3">
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div> -->
      <!-- /.social-auth-links -->
      @guest
       @if (Route::has('register'))
         <p class="mb-0"><a href="{{ route('register') }}" class="text-center" >{{ __('Register') }}</a></p>
        @endif
        @if (Route::has('password.request'))
       <!--  <a href="{{ route('password.request') }}" >
            {{ __('Forgot Your Password?') }}
        </a> -->
        @endif
        
       @endguest
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
</body>  
<script src=" {{ asset('assets/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src=" {{ asset('dist/js/adminlte.js') }}"></script>
</body>
</html>
