@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mt-3">
        <div class="col-sm-6">
          <h2 class="m-0">   User Notification</h2>
        </div><!-- /.col -->

        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Notification</li>
          </ol>
        </div> 
      </div> 
    </div> 
  </div>


  <section class="content">

    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card  ">
            @if(Session::has('success'))
            <div class="alert alert-success">
              <strong>Success!</strong> {{Session::get('success')}}
            </div>
            @elseif(Session::has('failed'))
            <div class="alert alert-success">
              <strong>Failed!</strong> {{Session::get('failed')}}
            </div>
            @endif
            <div class="card-body">

              <form method="post" action=" {{ route('userNotification') }}">
                {{ csrf_field() }}
                <div class="form-row">
                    <div class="col-md-6">
                     <div class="form-group">
                        <label class="small mb-1" for="inputFirstName">Manufacturer</label>
                        <input type="hidden" name="type" value="Notification">
                          <select class="form-control " name="category"  style="width: 100%;border: 1px solid #c1baba;"  autofocus>
                            <option value=" ">Select Manufacturer</option>
                            <option value="ALL">Send All</option>
                            @if (count($products))
                            @foreach ($products as $pd)  
                            <option value="{{ $pd['product_id']}}">{{ $pd['product_name']}}</option>
                            @endforeach
                            @else
                            <option>State Not Found</option> 
                            @endif
                          </select>
                        @if ($errors->has('category'))
                        <span class="text-danger">{{ $errors->first('category') }}</span>
                        @endif
                      </div>
                  </div>
                  
                </div>
                <div class="form-row">
                  <div class="col-md-6">
                     <div class="form-group">
                      <label for="exampleInputPassword1">Message(<span style="color: #db5252;font-size:15px;">Max. 100 char required</span>)</label>
                      <textarea  name="message" maxlength="100" class="form-control textareaCount" style="border: 1px solid #c1baba;"  ></textarea>
                        @if ($errors->has('message'))
                          <span class="text-danger">{{ $errors->first('message') }}</span>
                          @endif
                    </div>
                    <script type="text/javascript">
                        $('textarea').maxlength({
                             alwaysShow: true,
                            threshold: 10,
                            warningClass: "label label-success",
                            limitReachedClass: "label label-danger",
                            separator: ' out of ',
                            preText: 'You write ',
                            postText: ' chars.',
                            validate: true
                        });
                  </script>
                  </div>
                   

                </div>
                  
                <div class="form-row">
                  <div class="col-md-2">
                    <input type="submit" class="btn btn-primary btn-block" name="submit" value="Send"> 
                  </div>
                </div>
                <div class="form-group mt-4 mb-0"></div>
              </form>
            </div>
          </div>
        </div>
      </div>
     <div class="row">
         <div class="col-md-12">
              <div class="card mt-3">
                 <div class="card-body">
                   <div class="card-header">
                 

                </div>
                      <div class=" ">
                          <table id="" class="table table-striped table-bordered example" style="width:100%">
                          <thead>
                              <tr>
                                  <th>Sr NO</th>
                                  <th>Name</th>
                                  <th>Assign Role</th>
                                  <th>Active</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          
                          </table>

                      </div>
                  </div>
              </div>
        </div>
     </div>   
    </div>
  </div>
</div>
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Recipient:</label>
            <input type="text" class="form-control" id="recipient-name">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Message:</label>
            <textarea class="form-control" id="message-text"></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Send message</button>
      </div>
    </div>
  </div>
</div>
@endsection
