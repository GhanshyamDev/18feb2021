@extends('layouts.customer')

@section('content')
<div class="content-wrapper">
   <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-sm-6">
            <h1 class="m-0">  Notification</h1>
          </div><!-- /.col -->

          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div> 
        </div> 
      </div> 
    </div>


<section class="content">

 <div class="container-fluid">
     
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-right">
                  @if(count($notifications))
                    <button class="btn" style="border: 1px solid #007bff;">
                    <a href="#" id="mark-all" onclick="markRequestAll()">  Mark all as read
                     </a>
                  </button>
                @endif
              </div>

                <div class="card-body">
                    <table class="table table-striped   example">
              <thead>
                  <tr >
                      <th>  # </th>
                      <th>Message</th>
                      <th>Date</th>
                      <th class="text-center"> Status </th>
                   </tr>
              </thead>
              <tbody id="allbody">
                  
                <?php foreach ($notifications as $notification) { ?>
                <tr  id="row_{{ $notification->id }}" style="height:40px; ">
                <td> 1 </td>
                <td> {{ $notification->data['message'] }}</td>
                <td> {{ date('d M Y', strtotime($notification->created_at))  }}</td>
                <td>  <a href="#" class="float-right mark-as-read" onclick="sendMarkRequest('{{ $notification->id }}')" data-id="">  Mark as read   </a> </td>
                </tr>
                 <?php } ?>
                
              </tbody>
          </table>
                          
                </div>
            </div>
        </div>
    </div>
  </div>
 </div>
</div>

@endsection
 